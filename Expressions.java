import decaf.lexer.*;
import java.io.*;		// Needed for pushbackreader, inputstream
import java.util.*;		// Needed for Hashtable

class Expressions
{
  static  Lexer lexer;
  static  Tok token;
  static MutableInt zero;
  static int next = 0; // for allocation of temporary storage
  public static void main (String[] args) throws IOException
  {
     lexer = new Lexer
                (new PushbackReader
                        (new InputStreamReader (System.in), 1024));
    Expressions e = new Expressions();
    e.eval ();
  }
  void eval ()
  { 
    MutableInt p = new MutableInt(0);
    token = new Tok(lexer);
    token.getToken();
    Expr (p); // look for an expression
    if (token.get_class()!=token.EOF) 
      reject();
    else 
      accept();
  }
  
  void atom (String cls, int left, MutableInt right, int result)
  /*  Put out an atom with parameters:
      cls		atom Class - ADD, MUL
      left	left operand, for ADD, MUL
      right	right operand for ADD, MUL
      result	result of operation for ADD, MUL
 
   */
  {  
    System.out.println ( cls + " T" + left + " T" + right +  " T" + result) ; 
  }

  static int avail = 0;
  Integer alloc()
  { 
    return new Integer (++avail); 
  }

  void Expr (MutableInt p)
  { 
    MutableInt q = new MutableInt(0);
    if (token.get_class()==token.LPar || token.get_class()==token.IDENT
      || token.get_class()==token.NUM)
    { 
      Term (q); // apply rule 1
      Elist (q.get(),p);
    } // end rule 1
    else 
      reject();
  }
  void Elist (int p, MutableInt q)
  { 
    int s;
    MutableInt r = new MutableInt();
    if (token.get_class()==token.PLUS)
    { 
      token.getToken(); // apply rule 2
      Term (r);
      s = alloc();
      atom ("ADD", p, r, s); // put out atom
      Elist (s,q);
    } // end rule 2
    else 
      if (token.get_class()==token.EOF || token.get_class()==token.RPar)
        q.set(p); // rule 3
      else 
        reject();
  }
  void Term (MutableInt p)
  { 
    MutableInt q = new MutableInt();
    if (token.get_class()==token.LPar || token.get_class()==token.IDENT
      || token.get_class()==token.NUM)
    { 
      Factor (q); // apply rule 4
      Tlist (q.get(),p);
    } // end rule 4
    else 
      reject();
  }
  void Tlist (int p, MutableInt q)
  { 
    int inp, s;
    MutableInt r = new MutableInt();
    if (token.get_class()==token.MUL)
    { 
      token.getToken(); // apply rule 5
      inp = token.get_class();
      Factor (r);
      s = alloc();
      atom ("MULT", p, r, s);
      Tlist (s,q);
    } // end rule 5
    else 
      if (token.get_class()==token.PLUS || token.get_class()==token.RPar
        || token.get_class()==token.EOF)
        q.set (p); // rule 6
      else 
        reject();
  }
  void Factor (MutableInt p)
  { 
    if (token.get_class()==token.LPar)
    {
      token.getToken(); // apply rule 7
      Expr (p);
      if (token.get_class()==token.RPar)
        token.getToken();
     else 
       reject();
    } // end rule 7
    else 
      if (token.get_class()==token.IDENT || token.get_class()==token.NUM)
      { 
        p.set(alloc()); // apply rule 8
        token.getToken();
      } // end rule 8
      else 
        reject();
  }
     void accept() // Accept the input
   { 
       System.out.println ("accept"); 
   }
   void reject() // Reject the input
   { 
       System.out.println ("reject");
       System.exit(0); // terminate parser
   }
}