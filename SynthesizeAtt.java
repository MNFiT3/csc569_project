import decaf.lexer.*;
import java.io.*;		// Needed for pushbackreader, inputstream
import java.util.*;		// Needed for Hashtable

class SynthesizeAtt
{
  static  Lexer lexer;
  static  Tok token;
  static MutableInt zero;
  static int next = 0; // for allocation of temporary storage
  public static void main (String[] args) throws IOException
  {
     lexer = new Lexer
                (new PushbackReader
                        (new InputStreamReader (System.in), 1024));
    SynthesizeAtt e = new SynthesizeAtt();
    e.eval ();
  }
  void eval ()
  { 
    MutableInt p = new MutableInt(0);
    token = new Tok(lexer);
    token.getToken();
    Expr (p); // look for an expression
    System.out.println(p.value);
    if (token.get_class()!=token.EOF) 
      reject();
    else 
      accept();
  }
  
  void Expr (MutableInt p)
  { 
    MutableInt q = new MutableInt(0), r = new
    MutableInt(0); // Attributes q,r
    if (token.get_class()==token.PLUS || token.get_class()==token.MUL) // Operator?
      if (token.get_class()==token.PLUS) // apply rule 1
      { 
        token.getToken(); // read next token
        Expr(q);
        Expr(r);
        p.set (q.get() + r.get());
      } // end rule 1
      else // should be a *, apply rule 2
      { 
        token.getToken(); // read next token
        Expr(q);
        Expr(r);
        p.set (q.get() * r.get());
      } // end rule 2
      else 
        if (token.get_class()==token.NUM) // Is it a number?
        { 
           p.set (Integer.parseInt(token.getValue().trim())); // apply rule 3
           token.getToken(); // read next token
        } // end rule 3
        else 
          reject();
  }
       void accept() // Accept the input
   { 
       System.out.println ("accept"); 
   }
   void reject() // Reject the input
   { 
       System.out.println ("reject");
       System.exit(0); // terminate parser
   }
}